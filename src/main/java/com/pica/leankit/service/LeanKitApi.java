package com.pica.leankit.service;

import com.pica.leankit.domain.LeanKitBoard;
import com.pica.leankit.domain.LeanKitResponse;
import org.springframework.web.client.RestTemplate;

public class LeanKitApi {

    private String apiUrl;
    private RestTemplate restTemplate;

    public LeanKitApi(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public LeanKitApi(String apiUrl, String username, String password) {
        this(apiUrl);
        this.restTemplate = new BasicAuthRestTemplate(username, password);
    }

    public LeanKitBoard getBoard(String boardId) {
        String url = String.format("%s/Boards/%s", apiUrl, boardId);
        LeanKitBoardResponse response = restTemplate.getForObject(url, LeanKitBoardResponse.class);
        LeanKitAssert.isOk(response, url);
        return response.getReplyData().get(0);
    }

    private static class LeanKitBoardResponse extends LeanKitResponse<LeanKitBoard> {
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
}
