package com.pica.leankit.service;

public class LeanKitApiUrl {

    public static String forOrganization(String organization) {
        return String.format("http://%s.leankitkanban.com/Kanban/Api", organization);
    }
}
