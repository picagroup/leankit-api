package com.pica.leankit.service;

import com.pica.leankit.domain.LeanKitResponse;
import com.pica.leankit.exception.LeanKitException;

public class LeanKitAssert {


    public static void isOk(LeanKitResponse response, String url) {
        if (!response.isOk()) {
            throw new LeanKitException(url + " - " + response.getReplyText());
        }
    }
}
