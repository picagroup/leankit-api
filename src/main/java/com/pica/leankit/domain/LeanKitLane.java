package com.pica.leankit.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeanKitLane {

    @JsonProperty("Id")
    private Long id;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Cards")
    private List<LeanKitCard> cards;
    @JsonProperty("ParentLaneId")
    private Long parentLaneId;
    @JsonProperty("ChildLaneIds")
    private List<Long> childLaneIds;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<LeanKitCard> getCards() {
        return cards;
    }

    public Long getParentLaneId() {
        return parentLaneId;
    }

    public List<Long> getChildLaneIds() {
        return childLaneIds;
    }

    public boolean isTopLevel() {
        return parentLaneId == 0L;
    }
}
