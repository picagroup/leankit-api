package com.pica.leankit.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LeanKitBoard {

    @JsonProperty("Id")
    private Long id;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Lanes")
    private List<LeanKitLane> lanes;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<LeanKitLane> getLanes() {
        return lanes;
    }
}
