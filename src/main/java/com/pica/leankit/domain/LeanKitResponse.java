package com.pica.leankit.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class LeanKitResponse<T> {

    public static final int OK_REPLY_CODE = 200;

    @JsonProperty("ReplyData")
    private List<T> replyData;
    @JsonProperty("ReplyCode")
    private Integer replyCode;
    @JsonProperty("ReplyText")
    private String replyText;

    public List<T> getReplyData() {
        return replyData;
    }

    public Integer getReplyCode() {
        return replyCode;
    }

    public String getReplyText() {
        return replyText;
    }

    public boolean isOk() {
        return OK_REPLY_CODE == replyCode;
    }
}