package com.pica.leankit.exception;

public class LeanKitException extends RuntimeException {
    public LeanKitException(String s) {
        super(s);
    }
}
