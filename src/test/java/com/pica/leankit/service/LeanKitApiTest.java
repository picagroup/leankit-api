package com.pica.leankit.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.pica.leankit.domain.LeanKitBoard;
import com.pica.leankit.exception.LeanKitException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class LeanKitApiTest {

    private static WireMockServer wireMockServer;
    private static String wireMockBaseUrl;

    @BeforeClass
    public static void classSetUp() throws Exception {
        wireMockServer = new WireMockServer(wireMockConfig());
        wireMockServer.start();
        wireMockBaseUrl = "http://localhost:" + wireMockServer.port();
    }

    @Before
    public void setUp() throws Exception {
        WireMock.reset();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        wireMockServer.stop();
    }

    @Test
    public void getBoard_existingBoard_retrievesAndParsesBoardInfo() throws Exception {
        stubJsonRequestWithClasspathFileContents("/api/Boards/1", "json/boards/1.json");

        LeanKitApi api = createApi();
        LeanKitBoard board = api.getBoard("1");

        assertThat(board.getTitle(), equalTo("Getting Started With LeanKit template"));
    }

    @Test
    public void getBoard_noPermissionResponse_throwsException() throws Exception {
        stubJsonRequestWithClasspathFileContents("/api/Boards/2", "json/boards/NoPermission.json");

        LeanKitApi api = createApi();
        try {
            api.getBoard("2");
            fail("Expected exception");
        } catch (LeanKitException e) {
            assertThat(e.getMessage(), containsString("/api/Boards/2"));
            assertThat(e.getMessage(), containsString("You do not have the necessary permission to perform this action."));
        }
    }

    private LeanKitApi createApi() {
        LeanKitApi api = new LeanKitApi(wireMockBaseUrl + "/api");
        api.setRestTemplate(new RestTemplate());
        return api;
    }

    private void stubJsonRequestWithClasspathFileContents(String url, String filePath) throws IOException {
        Resource classPathResource = new ClassPathResource(filePath);
        stubFor(get(urlEqualTo(url))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader("Content-Type", "application/json")
                        .withBody(IOUtils.toString(classPathResource.getInputStream()))));
    }
}
